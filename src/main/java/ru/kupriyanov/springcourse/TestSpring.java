package ru.kupriyanov.springcourse;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.kupriyanov.springcourse.interfarce.IMusic;
import ru.kupriyanov.springcourse.models.MusicPlayer;

public class TestSpring {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        IMusic music1 = context.getBean("classicBean", IMusic.class);
        IMusic music2 = context.getBean("classicBean", IMusic.class);

        IMusic music3 = context.getBean("rockBean", IMusic.class);
        IMusic music4 = context.getBean("rockBean", IMusic.class);

//        MusicPlayer musicPlayer = new MusicPlayer(music);
//        MusicPlayer musicPlayer2 = new MusicPlayer(music2);

        System.out.println(music1 == music2); // одна и та же ссылка из-за scope singleton
        System.out.println(music3 == music4); // разные ссылки из-за scope prototype
        MusicPlayer musicPlayer = context.getBean("musicListPlayer", MusicPlayer.class);
        musicPlayer.playMusicList();
        context.close();
    }

}