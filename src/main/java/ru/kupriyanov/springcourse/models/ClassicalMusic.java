package ru.kupriyanov.springcourse.models;

import ru.kupriyanov.springcourse.interfarce.IMusic;

public final class ClassicalMusic implements IMusic {

    @Override
    public String getSong() {
        return "classical song";
    }

}
